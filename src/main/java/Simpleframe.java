import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Simpleframe {

    JFrame ramka = new JFrame("\u2630\u268C\u268A ");   //☂
    JPanel panel = new JPanel();
    JButton printListOfB = new JButton();
    JButton printListOfM = new JButton();
    JButton printListOfU = new JButton();
    JLabel labelB = new JLabel("Lista książek");
    JLabel labelM = new JLabel("Lista magazynów");
    JLabel labelU = new JLabel("Lista użytkowników");
    Font font = new Font("Tahoma", Font.BOLD, 14);
    Library library;

    public Simpleframe(Library library) {
        frameinit();
        this.library = library;
    }

    public void frameinit () {
        panelinit();
        ramka.add(panel);
        ramka.pack();
        ramka.setMinimumSize(new Dimension(300,210));
        ramka.setVisible(true);
        ramka.setLocationRelativeTo(null);
        ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public void panelinit() {
        panel.setLayout(new BorderLayout());
        buttonsinit();
        panel.add(printListOfB, BorderLayout.PAGE_START);
        panel.add(printListOfM, BorderLayout.CENTER);
        panel.add(printListOfU, BorderLayout.PAGE_END);
    }
    public void buttonsinit() {
        labelsinit();
        printListOfB.add(labelB);
        printListOfM.add(labelM);
        printListOfU.add(labelU);
        printListOfB.setPreferredSize(new Dimension(300,70));
        printListOfM.setPreferredSize(new Dimension(300,70));
        printListOfU.setPreferredSize(new Dimension(300,70));

        printListOfB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                library.printListOfBooks();
            }
        });
        printListOfM.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                library.printListOfMagazines();
            }
        });
        printListOfU.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                library.printListOfUsers();
            }
        });
    }

    public void labelsinit() {
        labelB.setFont(font);
        labelM.setFont(font);
        labelU.setFont(font);
        labelB.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelM.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelU.setAlignmentX(Component.CENTER_ALIGNMENT);
    }
}