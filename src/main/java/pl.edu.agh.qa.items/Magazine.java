package pl.edu.agh.qa.items;

public class Magazine extends Item {
    private final char type='M';
    String magazine_number;
    public Magazine (String title, String magazine_number) {
        super(title);
        this.magazine_number=magazine_number;
    }
    public Magazine (String title, String magazine_number, int quantity, int available_quantity) {
        super(title, quantity, available_quantity);
        this.magazine_number=magazine_number;
    }
    public void setMagazineNumber (String magazine_number) {
        this.magazine_number = magazine_number;
    }
    public String getMagazineNumber () {
        return magazine_number;
    }
    public char getType () {
        return type;
    }
}
