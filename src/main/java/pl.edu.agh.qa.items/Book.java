package pl.edu.agh.qa.items;

public class Book extends Item {
    private final char type='B';
    String author;
    public Book (String title, String author) {
        super(title);
        this.author=author;
    }
    public Book (String title, String author, int quantity, int available_quantity) {
        super(title, quantity, available_quantity);
        this.author=author;
    }
    public void setAuthor (String author) {
        this.author = author;
    }
    public String getAuthor () {
        return author;
    }
    public char getType () {
        return type;
    }
}
