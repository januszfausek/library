package pl.edu.agh.qa.items;

public class Item {
    private String title;
    private int quantity, available_quantity;
    private int amount;
    public Item () {}
    public Item (String title) {
        this.title = title;
    }
    public Item (String title, int quantity, int available_quantity) {
        this.title = title;
        this.quantity= quantity;
        this.available_quantity = available_quantity;
    }
    public void setTitle (String title) {
        this.title = title;
    }
    public void setQuantity (int quantity) {
        this.quantity = quantity;
    }
    public void setAvailableQuantity (int available_quantity) {
        this.available_quantity = available_quantity;
    }
    public void setAmount (int amount) {
        this.amount = amount;
    }
    public String getTitle () {
        return title;
    }
    public int getQuantity () {
        return quantity;
    }
    public int getAvailableQuantity () {
        return available_quantity;
    }
    public int getAmount() {
        return amount;
    }
}
