package pl.edu.agh.qa.users;

public class Lecturer extends User {
    private final char type='L';
    private final int max=10;
    private String[] borrowed = new String [max];

    public Lecturer (String name, String surname) {
        super(name, surname);
        for ( int i=0; i<max; i++) {
            borrowed[i]="-";
        }
    }
    public void setBorrowed(int a, String b) {
        borrowed[a]=b;
    }
    public char getType () {
        return type;
    }
    public int getMax () {
        return max;
    }
    public String[] getBorrowed() {
        return borrowed;
    }
}
