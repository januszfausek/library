package pl.edu.agh.qa.users;

public class Student extends User {
    private final char type='S';
    private final int max=4;
    private String[] borrowed = new String [max];

    public Student (String name, String surname) {
        super(name, surname);
        for ( int i=0; i<max; i++) {
            borrowed[i]="-";
        }
    }
    public void setBorrowed(int a, String b) {
        borrowed[a]=b;
    }

    public char getType () {
        return type;
    }
    public int getMax () {
        return max;
    }
    public String[] getBorrowed() {
        return borrowed;
    }
}
