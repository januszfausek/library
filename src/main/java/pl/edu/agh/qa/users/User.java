package pl.edu.agh.qa.users;

public class User {
    private String name, surname;
    private int cardnumber;
    private int numberOfItemsBorrowed;

    public User () {}
    public User (String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    public void setName (String name) {
        this.name = name;
    }
    public void setSurname (String surname) {
        this.surname = surname;
    }
    public void setCardnumber (int cardnumber) {
        this.cardnumber = cardnumber;
    }
    public void setNumberOfItemsBorrowed (int numberOfItemsBorrowed) {
        this.numberOfItemsBorrowed = numberOfItemsBorrowed;
    }
    public String getName () {
        return name;
    }
    public String getSurname () {
        return surname;
    }
    public int getCardnumber () {
        return cardnumber;
    }
    public int getNumberOfItemsBorrowed () {
        return numberOfItemsBorrowed;
    }
}
