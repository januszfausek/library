import pl.edu.agh.qa.items.Book;
import pl.edu.agh.qa.items.Item;
import pl.edu.agh.qa.items.Magazine;
import pl.edu.agh.qa.users.Lecturer;
import pl.edu.agh.qa.users.Student;
import pl.edu.agh.qa.users.User;

import java.io.*;
//import java.io.BufferedWriter;
//import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {
    File libraryfile, usersfile;
    List<Item> itemsoflibrary = new ArrayList<>();
    List<User> usersoflibrary = new ArrayList<>();
    private static int linecounter, additemtoLcheck, updateLitcheck, updateLuscheck, additemtoL_while_check;
    Item item;
    User user;

    public Library() { }

    public Library(File libraryfile, File usersfile) {
        this.libraryfile = libraryfile;
        this.usersfile = usersfile;
        FileReader fr;
        BufferedReader br;

        // wczytanie do pamięci pozycji z katalogu
        try {
            fr = new FileReader(libraryfile);
            br = new BufferedReader(fr);
            String row;
            while ((row = br.readLine()) != null) {
                String[] templine = row.split(";");
                if (templine[0].equals("B")) {
                    item = new Book(templine[1], templine[2], Integer.parseInt(templine[3]), Integer.parseInt(templine[4]));
                    itemsoflibrary.add(item);
                    //System.out.println(((Book) item).getType() + " " + item.getTitle() + " " + ((Book) item).getAuthor() + " " + item.getQuantity() + " " + item.getAvailableQuantity());
                }
                if (templine[0].equals("M")) {
                    item = new Magazine(templine[1], templine[2], Integer.parseInt(templine[3]), Integer.parseInt(templine[4]));
                    itemsoflibrary.add(item);
                    //System.out.println(((Magazine) item).getType() + " " + item.getTitle() + " " + ((Magazine) item).getMagazineNumber() + " " + item.getQuantity() + " " + item.getAvailableQuantity());
                }
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // wczytanie do pamięci użytkowników
        try {
            fr = new FileReader(usersfile);
            br = new BufferedReader(fr);
            String row;
            while ((row = br.readLine()) != null) {
                String[] templine = row.split(";");
                if (templine[3].equals("S")) {
                    user = new Student(templine[0], templine[1]);
                    user.setCardnumber(Integer.parseInt(templine[2]));
                    user.setNumberOfItemsBorrowed(Integer.parseInt(templine[4]));
                    for (int i=0; i<((Student) user).getMax(); i++) {
                        ((Student)user).setBorrowed(i, templine[i+5]);
                    }
                    usersoflibrary.add(user);
                    //System.out.println(user.getName() + " " + user.getSurname() + " " + user.getCardnumber() + " " + ((Student) user).getType() + " " + user.getNumberOfItemsBorrowed() + " " + ((Student) user).getBorrowed()[3]);
                }
                if (templine[3].equals("L")) {
                    user = new Lecturer(templine[0], templine[1]);
                    user.setCardnumber(Integer.parseInt(templine[2]));
                    user.setNumberOfItemsBorrowed(Integer.parseInt(templine[4]));
                    for (int i=0; i<((Lecturer) user).getMax(); i++) {
                        ((Lecturer)user).setBorrowed(i, templine[i+5]);
                    }
                    usersoflibrary.add(user);
                    //System.out.println(user.getName() + " " + user.getSurname() + " " + user.getCardnumber() + " " + ((Lecturer) user).getType() + " " + user.getNumberOfItemsBorrowed()+ " " + ((Lecturer) user).getBorrowed()[9]);
                }
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User create_new_user() {
        User user = new User(" ", " ");
        String name, surname;
        char utype;
        FileReader fr;
        BufferedReader br;
        linecounter=1;

        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Podaj typ użytkownika: S-student, L-wykładowca ");
            utype = sc.next().charAt(0);
        } while (!((utype == 'L') || (utype == 'S')));
        String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
        System.out.println("Podaj imię: ");
        name = sc.nextLine();
        System.out.println("Podaj nazwisko: ");
        surname = sc.nextLine();

        try {
            fr = new FileReader(usersfile);
            br = new BufferedReader(fr);
            while ((br.readLine()) != null) {
                linecounter++;
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (utype == 'L') {
            user = new Lecturer(name, surname);
        }
        if (utype == 'S') {
            user = new Student(name, surname);
        }
        user.setCardnumber(linecounter);
        //System.out.println(((Student)user).getBorrowed()[0]);
        return user;
    }

    public void addUserToLibrary(User user) {
        FileWriter fw;
        BufferedWriter bw;
        try {
            fw = new FileWriter(usersfile, true);
            bw = new BufferedWriter(fw);
            if (usersfile.length() != 0) bw.newLine();
            if (user instanceof Lecturer) {
                bw.write(user.getName() + ";" + user.getSurname() + ";" + user.getCardnumber() + ";" + ((Lecturer) user).getType() + ";" + 0);
                for (int i = 0; i < ((Lecturer) user).getMax(); i++) bw.write(";" + ((Lecturer) user).getBorrowed()[i]);
            }
            if (user instanceof Student) {
                bw.write(user.getName() + ";" + user.getSurname() + ";" + user.getCardnumber() + ";" + ((Student) user).getType() + ";" + 0);
                for (int i = 0; i < ((Student) user).getMax(); i++) bw.write(";" + ((Student) user).getBorrowed()[i]);
            }
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        usersoflibrary.add(user);
    }

    public Item createNewItem() {
        Item item = new Item("");
        String title, second_field;
        char itype;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Rodzaj pozycji: B-książka, M-magazyn");
            itype = sc.next().charAt(0);
        } while (!((itype == 'B') || (itype == 'M')));
        String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
        System.out.println("Podaj tytuł: ");
        title = sc.nextLine();
        if (itype == 'B') {
            System.out.println("Podaj imię i nazwisko autora oddzielone spacją: ");
            second_field = sc.nextLine();
            item = new Book(title, second_field);
        }
        if (itype == 'M') {
            System.out.println("Podaj numer magazynu: ");
            second_field = sc.nextLine();
            item = new Magazine(title, second_field);
        }
        System.out.println("Podaj ilość egzemplarzy: ");
        item.setAmount(sc.nextInt());
        return item;
    }

    public void addItemToLibrary(Item item) {
        additemtoL_while_check = 1;
        Library templibrary = new Library();
        char answer;
        Scanner sc = new Scanner(System.in);
        while (additemtoL_while_check == 1) {
            additemtoLcheck = 0;
            if (item instanceof Book) {
                for (Item it : itemsoflibrary) {
                    if (it instanceof Book) {
                        if (it.getTitle().equals(item.getTitle()) && ((Book) it).getAuthor().equals(((Book) item).getAuthor())) {
                            it.setQuantity(it.getQuantity() + item.getAmount());
                            it.setAvailableQuantity(it.getAvailableQuantity() + item.getAmount());
                            //System.out.println(((Book) item).getType() + " " + item.getTitle() + " " + ((Book) item).getAuthor() + " " + it.getQuantity() + " " + it.getAvailableQuantity());
                            updateLibraryItems();
                            additemtoLcheck = 1;
                            break;
                        }
                    }
                }
            }
            if (item instanceof Magazine) {
                for (Item it : itemsoflibrary) {
                    if (it instanceof Magazine) {
                        if (it.getTitle().equals(item.getTitle()) && ((Magazine) it).getMagazineNumber().equals(((Magazine) item).getMagazineNumber())) {
                            it.setQuantity(it.getQuantity() + item.getAmount());
                            it.setAvailableQuantity(it.getAvailableQuantity() + item.getAmount());
                            //System.out.println(((Magazine) item).getType() + " " + item.getTitle() + " " + ((Magazine) item).getMagazineNumber() + " " + it.getQuantity() + " " + it.getAvailableQuantity());
                            updateLibraryItems();
                            additemtoLcheck = 1;
                            break;
                        }
                    }
                }
            }
            if (additemtoLcheck == 0) {
                FileWriter fw;
                BufferedWriter bw;
                try {
                    fw = new FileWriter(libraryfile, true);
                    bw = new BufferedWriter(fw);
                    if (libraryfile.length() != 0) bw.newLine();
                    item.setQuantity(item.getQuantity() + item.getAmount());
                    item.setAvailableQuantity(item.getAvailableQuantity() + item.getAmount());
                    if (item instanceof Book)
                        bw.write("B" + ";" + item.getTitle() + ";" + ((Book) item).getAuthor() + ";" + item.getQuantity() + ";" + item.getAvailableQuantity());
                    if (item instanceof Magazine)
                        bw.write("M" + ";" + item.getTitle() + ";" + ((Magazine) item).getMagazineNumber() + ";" + item.getQuantity() + ";" + item.getAvailableQuantity());
                    //System.out.println(user instanceof Student);
                    bw.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                itemsoflibrary.add(item);
            }
            System.out.println("Zapisać kolejną pozycję? T N ");
            do {
                answer = sc.next().charAt(0);
                String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
            } while (!((answer == 'T') || (answer == 'N')));
            if (answer == 'T')  item = templibrary.createNewItem();
            if (answer == 'N') additemtoL_while_check = 0;
        }
    }

    public void updateLibraryItems() {
        // Zapis wszystkich pozycji do pliku po dokonaniu jakiejkolwiek zmiany (nadpisanie)
        updateLitcheck = 0;
        FileWriter fw;
        BufferedWriter bw;
        try {
            fw = new FileWriter(libraryfile);
            bw = new BufferedWriter(fw);
            for (Item it2 : itemsoflibrary) {
                updateLitcheck++;
                if (it2 instanceof Book) {
                    bw.write(((Book) it2).getType() + ";" + it2.getTitle() + ";" + ((Book) it2).getAuthor() + ";" + it2.getQuantity() + ";" + it2.getAvailableQuantity());
                    if (updateLitcheck != itemsoflibrary.size()) bw.newLine();
                }
                if (it2 instanceof Magazine) {
                    bw.write(((Magazine) it2).getType() + ";" + it2.getTitle() + ";" + ((Magazine) it2).getMagazineNumber() + ";" + it2.getQuantity() + ";" + it2.getAvailableQuantity());
                    if (updateLitcheck != itemsoflibrary.size()) bw.newLine();
                }
            }
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateLibraryUsers() {
        // Zapis wszystkich użytkowników do pliku po dokonaniu jakiejkolwiek zmiany (nadpisanie)
        updateLuscheck=0;
        FileWriter fw;
        BufferedWriter bw;
        try {
            fw = new FileWriter(usersfile);
            bw = new BufferedWriter(fw);
            for (User u : usersoflibrary) {
                updateLuscheck++;
                if (u instanceof Lecturer) {
                    bw.write(u.getName() + ";" + u.getSurname() + ";" + u.getCardnumber() + ";" + ((Lecturer) u).getType() + ";" + u.getNumberOfItemsBorrowed());
                    for (int i=0; i<((Lecturer) u).getMax(); i++) bw.write(";" + ((Lecturer) u).getBorrowed()[i]);
                    if (updateLuscheck != usersoflibrary.size()) bw.newLine();
                }
                if (u instanceof Student) {
                    bw.write(u.getName() + ";" + u.getSurname() + ";" + u.getCardnumber() + ";" + ((Student) u).getType() + ";" + u.getNumberOfItemsBorrowed());
                    for (int i=0; i<((Student) u).getMax(); i++) bw.write(";" + ((Student) u).getBorrowed()[i]);
                    if (updateLuscheck != usersoflibrary.size()) bw.newLine();
                }
            }
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User findUser(int cardnumb) {
        User user = new User();
        for (User u : usersoflibrary) {
            if (u.getCardnumber() == cardnumb) {
                user = u;
                break;
            }
        }
        return user;
    }

    public Item findItem(String title, String author_or_number) {
        Item item = new Item();
        for (Item it : itemsoflibrary) {
            if (it instanceof Book) {
                if ((it.getTitle().equals(title)) && (((Book) it).getAuthor().equals(author_or_number) )) {
                    item = it;
                    break;
                }
            }
            if (it instanceof Magazine) {
                if ((it.getTitle().equals(title)) && (((Magazine) it).getMagazineNumber().equals(author_or_number))) {
                    item = it;
                    break;
                }
            }
        }
        return item;
    }

    public boolean rentItemToUser(Item item, User user) {
        boolean result=true;
        if (user instanceof Student) {
            if ((((Student) user).getMax() - user.getNumberOfItemsBorrowed()) <= 0) {
                System.out.println("Wykorzystano limit wypożyczeń");
                result = false;
            } else {
                if (item.getAvailableQuantity() > 0) {
                    if (item instanceof Book) ((Student)user).setBorrowed(user.getNumberOfItemsBorrowed(), item.getTitle()+"-"+((Book)item).getAuthor());
                    if (item instanceof Magazine) ((Student)user).setBorrowed(user.getNumberOfItemsBorrowed(), item.getTitle()+"-"+((Magazine)item).getMagazineNumber());
                    //System.out.println(((Student)user).getBorrowed()[0]);
                    item.setAvailableQuantity(item.getAvailableQuantity() - 1);
                    user.setNumberOfItemsBorrowed(user.getNumberOfItemsBorrowed() + 1);
                    System.out.println("Użytkownik może wypożyczyć jeszcze maksymalnie " + (((Student) user).getMax() - user.getNumberOfItemsBorrowed()) + " pozycje");
                    updateLibraryItems();
                    updateLibraryUsers();
                    result = true;
                } else {
                    System.out.println("Pozycja niedostępna");
                    result = false;
                }

            }
        }
        if (user instanceof Lecturer) {
            if ((((Lecturer) user).getMax() - user.getNumberOfItemsBorrowed()) <= 0) {
                System.out.println("Wykorzystano limit wypożyczeń");
                result = false;
            } else {
                if (item.getAvailableQuantity() > 0) {
                    if (item instanceof Book) ((Lecturer)user).setBorrowed(user.getNumberOfItemsBorrowed(), item.getTitle()+"-"+((Book)item).getAuthor());
                    if (item instanceof Magazine) ((Lecturer)user).setBorrowed(user.getNumberOfItemsBorrowed(), item.getTitle()+"-"+((Magazine)item).getMagazineNumber());
                    //System.out.println(((Lecturer)user).getBorrowed()[0]);
                    item.setAvailableQuantity(item.getAvailableQuantity() - 1);
                    user.setNumberOfItemsBorrowed(user.getNumberOfItemsBorrowed() + 1);
                    System.out.println("Użytkownik może wypożyczyć jeszcze maksymalnie " + (((Lecturer) user).getMax() - user.getNumberOfItemsBorrowed()) + " pozycje");
                    updateLibraryItems();
                    updateLibraryUsers();
                    result = true;
                } else {
                    System.out.println("Pozycja niedostępna");
                    result = false;
                }
            }
        }
        return result;
    }

    public void printListOfMagazines() {
        System.out.println("*** Spis magazynów: ***");
        for (Item magazine: itemsoflibrary) {
            if (magazine instanceof Magazine) System.out.println(magazine.getTitle() + ";" + ((Magazine) magazine).getMagazineNumber() + ";" + magazine.getQuantity() + ";" + magazine.getAvailableQuantity());
        }
    }
    public void printListOfBooks() {
        System.out.println("*** Spis książek: ***");
        for (Item book: itemsoflibrary) {
            if (book instanceof Book) System.out.println(book.getTitle() + ";" + ((Book) book).getAuthor() + ";" + book.getQuantity() + ";" + book.getAvailableQuantity());
        }
    }

    public void printListOfUsers() {
        System.out.println("*** Spis użytkowników: ***");
        for (User us: usersoflibrary) {
            if (us instanceof Student) System.out.println(us.getName() + ";" + us.getSurname() + ";" + us.getCardnumber() + ";" + ((Student)us).getType());
            if (us instanceof Lecturer) System.out.println(us.getName() + ";" + us.getSurname() + ";" + us.getCardnumber() + ";" + ((Lecturer)us).getType());
        }
    }

    public void importItemsFromFile(String csvFile) {
        boolean checker;
        FileReader fr;
        BufferedReader br;
        try {
            fr = new FileReader(csvFile);
            br = new BufferedReader(fr);
            String row;
            while ((row = br.readLine()) != null) {
                checker=false;
                String[] templine = row.split(";");
                if (templine[3].equals("B")) {
                    for (Item it : itemsoflibrary) {
                        if (it instanceof Book) {
                            if (it.getTitle().equals(templine[0]) && ((Book) it).getAuthor().equals(templine[1])) {
                                it.setQuantity(it.getQuantity() + Integer.parseInt(templine[2]));
                                it.setAvailableQuantity(it.getAvailableQuantity() + Integer.parseInt(templine[2]));
                                updateLibraryItems();
                                checker=true;
                                break;
                            }
                        }
                    }
                    if (!checker) itemsoflibrary.add(new Book(templine[0], templine[1], Integer.parseInt(templine[2]), Integer.parseInt(templine[2])));
                    updateLibraryItems();
                }
                if (templine[3].equals("M")) {
                    for (Item it : itemsoflibrary) {
                        if (it instanceof Magazine) {
                            if (it.getTitle().equals(templine[0]) && ((Magazine) it).getMagazineNumber().equals(templine[1])) {
                                it.setQuantity(it.getQuantity() + Integer.parseInt(templine[2]));
                                it.setAvailableQuantity(it.getAvailableQuantity() + Integer.parseInt(templine[2]));
                                updateLibraryItems();
                                checker=true;
                                break;
                            }
                        }
                    }
                    if (!checker) itemsoflibrary.add(new Magazine(templine[0], templine[1], Integer.parseInt(templine[2]), Integer.parseInt(templine[2])));
                    updateLibraryItems();
                }
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exportUsersWithItemsToFile (String csvFile){
        List<Integer> temp = new ArrayList<>();
        FileWriter fw;
        BufferedWriter bw;
        try {
            fw = new FileWriter(csvFile);
            bw = new BufferedWriter(fw);
            for (User us3 : usersoflibrary) {
                if (us3 instanceof Lecturer) {
                    for (int i = 0; i < ((Lecturer) us3).getMax(); i++) {
                        if (!(((Lecturer) us3).getBorrowed()[i].equals("-"))) {
                            temp.add(i);
                        }
                    }
                    if (temp.size()!=0) {
                        bw.write(us3.getCardnumber() + "[");
                        for (int i = 0; i < temp.size(); i++) {
                            if (i!=(temp.size() - 1)) bw.write(((Lecturer) us3).getBorrowed()[temp.get(i)] + ";");
                            else {
                                bw.write(((Lecturer) us3).getBorrowed()[temp.get(i)]);
                            }
                        }
                        bw.write("]");
                        bw.newLine();
                    }
                    temp.clear();
                }
                if (us3 instanceof Student) {
                    for (int i = 0; i < ((Student) us3).getMax(); i++) {
                        if (!(((Student) us3).getBorrowed()[i].equals("-"))) temp.add(i);
                    }
                    if (temp.size()!=0) {
                        bw.write(us3.getCardnumber() + "[");
                        for (int i = 0; i < temp.size(); i++) {
                            if (i!=(temp.size() - 1)) bw.write(((Student) us3).getBorrowed()[temp.get(i)] + ";");
                            else {
                                bw.write(((Student) us3).getBorrowed()[temp.get(i)]);
                            }
                        }
                        bw.write("]");
                        bw.newLine();
                    }
                    temp.clear();
                }
            }
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

        public static void main (String[] args) {
            File file1 = new File("Library.lb");
            File file2 = new File("Users.lb");
            Library library = new Library(file1, file2);
            char answer;
            int answer2;
            String answer3, answer4, answer5;
            Scanner sc = new Scanner(System.in);


            do {
                System.out.println("Zapisać nowego użytkownika? T N ");
                answer = sc.next().charAt(0);
                // String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
            } while (!((answer == 'T') || (answer == 'N')));
            while (answer == 'T') {
                library.addUserToLibrary(library.create_new_user());
                System.out.println("Zapisać kolejnego użytkownika? T N ");
                do {
                    answer = sc.next().charAt(0);
                    // String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
                } while (!((answer == 'T') || (answer == 'N')));
            }

            do {
                System.out.println("Zapisać nową pozycję w katalogu? T N ");
                answer = sc.next().charAt(0);
                String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
            } while (!((answer == 'T') || (answer == 'N')));
            if (answer == 'T') library.addItemToLibrary(library.createNewItem());

            do {
                System.out.println("Wypożyczyć pozycję z katalogu? T N ");
                answer5 = sc.nextLine();
                // String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
            } while (!((answer5.equals("T")) || (answer5.equals("N"))));
            while (answer5.equals("T")) {
                System.out.println("Proszę podać numer karty: ");
                answer2 = sc.nextInt();
                //System.out.println("answer2 nr karty= " + answer2);
                String temp = sc.nextLine();
                System.out.println("Proszę podać tytuł: ");
                answer3 = sc.nextLine();
                //System.out.println("answer3 tytul= " + answer3);
                System.out.println("Proszę podać autora lub numer magazynu: ");
                answer4 = sc.nextLine();
                //System.out.println("answer4 autor/nrM = " + answer4);
                library.rentItemToUser(library.findItem(answer3, answer4), library.findUser(answer2));
                do {
                    System.out.println("Wypożyczyć kolejną pozycję z katalogu? T N ");
                    answer5 = sc.nextLine();
                    // String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
                } while (!((answer5.equals("T")) || (answer5.equals("N"))));
            }
            library.importItemsFromFile("Nowepozycje.txt");
            Simpleframe simpleframe = new Simpleframe(library);
            library.exportUsersWithItemsToFile("Borrowed.txt");
        }
}


// -------------------------------------------------------------------------------------------------------------
/*
do {
            System.out.println("Zapisać nową pozycję w katalogu? T N ");
            answer = sc.next().charAt(0);
            // String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
        } while (!((answer=='T') || (answer=='N')));
        while (answer=='T') {
            library.addItemToLibrary(library.createNewItem());
            System.out.println("Zapisać kolejną pozycję? T N ");
            do {
                answer = sc.next().charAt(0);
                // String temp = sc.nextLine(); // bo wczytuje automatycznie pusty string po użyciu charAt
            } while (!((answer=='T') || (answer=='N')));
        }
 */
